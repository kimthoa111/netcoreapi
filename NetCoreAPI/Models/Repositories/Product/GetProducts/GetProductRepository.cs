﻿using Microsoft.EntityFrameworkCore;
using NetCoreAPI.Data;

namespace NetCoreAPI.Models.Repositories.Product.GetProducts
{
    public class GetProductRepository : IGetProductsRepository
    {
        private readonly ApplicationDbContext _context;

        public GetProductRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Product>> GetProductsAsync()
        {
            return await _context.Products.ToListAsync();
        }
    }
}

