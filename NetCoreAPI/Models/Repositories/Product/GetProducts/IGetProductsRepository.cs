﻿namespace NetCoreAPI.Models.Repositories.Product.GetProducts
{
    public interface IGetProductsRepository
    {
        Task<IEnumerable<Product>> GetProductsAsync();
    }
}
