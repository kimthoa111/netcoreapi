﻿using Microsoft.EntityFrameworkCore;

namespace NetCoreAPI.Models.Repositories.Product
{
    public class ProductRepository : IProductRepository
    {
        public async Task<IEnumerable<Product>> GetProductsAsync()
        {
            throw new NotImplementedException();
        }
        public async Task<Product> GetProductByIdAsync(int id)
        {
            throw new NotImplementedException();
        }
        public async Task AddProductAsync(Product product)
        {
            throw new NotImplementedException();
        }
        public async Task UpdateProductAsync(Product product)
        {
            throw new NotImplementedException();
        }
        public async Task DeleteProductAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
